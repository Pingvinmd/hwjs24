const board = document.getElementById("board");
const minesLeft = document.getElementById("minesLeft");
const restart = document.getElementById("restart");
const minesTotal = document.getElementById("minesTotal");

let size = 7;
let minesCount = Math.floor((size * size) / 6);
let openedCells = 0;
let flagsCount = 0;

createBoard();
placeMines();

function createBoard() {
  board.innerHTML = "";
  for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
      const cell = document.createElement("div");
      cell.classList.add("cell");
      cell.dataset.row = i;
      cell.dataset.col = j;
      cell.addEventListener("click", handleCellClick);
      cell.addEventListener("contextmenu", handleCellRightClick);
      board.appendChild(cell);
    }
  }
}

function placeMines() {
  let minesPlaced = 0;
  while (minesPlaced < minesCount) {
    const row = Math.floor(Math.random() * size);
    const col = Math.floor(Math.random() * size);
    const cell = board.querySelector(`[data-row="${row}"][data-col="${col}"]`);
    if (!cell.dataset.hasMine) {
      cell.dataset.hasMine = true;
      minesPlaced++;
    }
  }
  minesTotal.innerHTML = minesCount;
}

function handleCellClick(event) {
  const cell = event.target;
  if (cell.classList.contains("open") || cell.classList.contains("flag")) {
    return;
  }
  if (cell.dataset.hasMine) {
    gameOver();
    return;
  }
  openCell(cell);
}

function openCell(cell) {
  cell.classList.add("open");
  openedCells++;
  const mineCount = getMineCount(cell);
  if (mineCount) {
    cell.innerHTML = mineCount;
  } else {
    openAdjacentCells(cell);
  }
  checkWin();
}

function getMineCount(cell) {
  let mineCount = 0;
  const row = parseInt(cell.dataset.row);
  const col = parseInt(cell.dataset.col);
  for (let i = row - 1; i <= row + 1; i++) {
    for (let j = col - 1; j <= col + 1; j++) {
      if (i >= 0 && j >= 0 && i < size && j < size) {
        const adjacentCell = board.querySelector(`[data-row="${i}"][data-col="${j}"]`);
        if (adjacentCell.dataset.hasMine) {
          mineCount++;
        }
      }
    }
  }
  return mineCount;
}

function openAdjacentCells(cell) {
  const row = parseInt(cell.dataset.row);
  const col = parseInt(cell.dataset.col);
  for (let i = row - 1; i <= row + 1; i++) {
    for (let j = col - 1; j <= col + 1; j++) {
      if (i >= 0 && j >= 0 && i < size && j < size) {
        const adjacentCell = board.querySelector(`[data-row="${i}"][data-col="${j}"]`);
        if (!adjacentCell.classList.contains("open")) {
          openCell(adjacentCell);
        }
      }
    }
  }
}

function handleCellClick(e) {
  const cell = e.target;
  if (cell.classList.contains("flag") || cell.classList.contains("open")) {
  return;
  }
  if (cell.dataset.hasMine) {
  gameOver();
  return;
  }
  openCell(cell);
  }
  
  function handleCellRightClick(e) {
  e.preventDefault();
  const cell = e.target;
  if (cell.classList.contains("open")) {
  return;
  }
  if (cell.classList.contains("flag")) {
  cell.classList.remove("flag");
  flagsCount--;
  } else {
  cell.classList.add("flag");
  flagsCount++;
  }
  minesLeft.innerHTML = minesCount - flagsCount;
  checkWin();
  }
  
  function gameOver() {
  alert("Game Over");
  location.reload();
  }
  
  function checkWin() {
  if (openedCells === size * size - minesCount) {
  alert("You Win!");
  location.reload();
  }
  }
  
  restart.addEventListener("click", () => {
  location.reload();
  });
  
  document.getElementById("size").addEventListener("change", (event) => {
  size = parseInt(event.target.value);
  minesCount = Math.floor((size * size) / 6);
  minesTotal.innerHTML = minesCount;
  flagsCount = 0;
  openedCells = 0;
  createBoard();
  placeMines();
  });
  
  window.addEventListener("load", () => {
  createBoard();
  placeMines();
  });